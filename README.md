**Write an application that divides numbers and prints result into console.**
-----------------------------------------------------------------------------

Example of result:

![ExampleOfResult](/uploads/6db3c8570f97bd989acba96e209ad2ab/ExampleOfResult.jpg)


**Java version**
-----------------------------------------------------------------------------

java version "16.0.2" 2021-07-20

Java(TM) SE Runtime Environment (build 16.0.2+7-67)

Java HotSpot(TM) 64-Bit Server VM (build 16.0.2+7-67, mixed mode, sharing)

**Gradle version**
-----------------------------------------------------------------------------

***Gradle 7.2***

Build time:   2021-08-17 09:59:03 UTC

Revision:     a773786b58bb28710e3dc96c4d1a7063628952ad


Kotlin:       1.5.21

Groovy:       3.0.8

Ant:          Apache Ant(TM) version 1.10.9 compiled on September 27 2020

JVM:          15.0.2 (Oracle Corporation 15.0.2+7-27)

OS:           Windows 10 10.0 amd64

**Application run examples**
-----------------------------------------------------------------------------

![first_run_application](/uploads/c405066d409a75ec6692a5e7cd948767/first_run_application.jpg)

![second_run_application](/uploads/5ce815763e1251aa8913a4851047080a/second_run_application.jpg)

![third__run_application](/uploads/efc6f3e02f89f75ad75d926170c9e2e6/third__run_application.jpg)

**Used by Runner in this project**
-----------------------------------------------------------------------------

concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "division-runner"<br />
  url = "https://gitlab.com/"<br />
  token = "*Insert_here_your_token*"<br />
  executor = "docker"<br />
  environment = ["DOCKER_TLS_CERTDIR="]<br />
  [runners.custom_build_dir]<br />
  [runners.cache]<br />
    [runners.cache.s3]<br />
    [runners.cache.gcs]<br />
    [runners.cache.azure]<br />
  [runners.docker]<br />
    tls_verify = false<br />
    image = "docker:latest"<br />
    privileged = true<br />
    disable_entrypoint_overwrite = false<br />
    oom_kill_disable = false<br />
    disable_cache = false<br />
    volumes = ["/.m2", "/cache"]<br />
    shm_size = 0<br />
    
**Useful links**   
-----------------------------------------------------------------------------
**Gradla**<br />
 https://docs.gradle.org/current/userguide/userguide.html <br />
 https://docs.gradle.org/current/userguide/compatibility.html <br />
 https://habr.com/ru/company/yota/blog/546664/ <br />
 https://spring.io/guides/gs/gradle/ <br />

**Java Reflection API**<br />
https://javadevblog.com/polnoe-rukovodstvo-po-java-reflection-api-refleksiya-na-primerah.html <br />
https://javarush.ru/groups/posts/2177-primerih-ispoljhzovanija-reflection <br />
https://www.youtube.com/watch?v=7eYQPNNxLSM <br />
