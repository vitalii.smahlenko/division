package com.gmail.smaglenko.division.text;

import com.gmail.smaglenko.division.model.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FormatterTest {
    private Result result;
    private Formatter formatter;

    @BeforeEach
    public void setUp() {
        this.result = new Result();
        this.formatter = new Formatter(this.result);
    }

    @Test
    void format_shouldEqualsResult_whenDivisorOneDigitNumber() {
        String expectedResult = ReadTestDataFile("result_78454_4.txt");
        result.setDividend(78454);
        result.setDivisor(4);
        result.setQuotient(19613);
        result.setPartialDividend(new int[]{7, 38, 24, 5, 14});
        result.setProduct(new int[]{4, 36, 24, 4, 12});
        result.setRemainder("2");

        String actualResult = formatter.format();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void format_shouldEqualsResult_whenDivisorTwoDigitNumber() {
        String expectedResult = ReadTestDataFile("result_972210_45.txt");
        result.setDividend(972210);
        result.setDivisor(45);
        result.setQuotient(21604);
        result.setPartialDividend(new int[]{97, 72, 272, 210});
        result.setProduct(new int[]{90, 45, 270, 180});
        result.setRemainder("30");

        String actualResult = formatter.format();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void format_shouldEqualsResult_whenDivisorThreeDigitNumber() {
        String expectedResult = ReadTestDataFile("result_630440_610.txt");
        result.setDividend(630440);
        result.setDivisor(610);
        result.setQuotient(1033);
        result.setPartialDividend(new int[]{630, 2044, 2140});
        result.setProduct(new int[]{610, 1830, 1830});
        result.setRemainder("310");

        String actualResult = formatter.format();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void format_shouldEqualsResult_whenDivisorFourDigitNumber() {
        String expectedResult = ReadTestDataFile("result_12351234_1234.txt");
        result.setDividend(12351234);
        result.setDivisor(1234);
        result.setQuotient(10009);
        result.setPartialDividend(new int[]{1235, 11234});
        result.setProduct(new int[]{1234, 11106});
        result.setRemainder("128");

        String actualResult = formatter.format();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void format_shouldEqualsResult_whenRemainderStartsZero() {
        String expectedResult = ReadTestDataFile("result_12340035_1234.txt");
        result.setDividend(12340035);
        result.setDivisor(1234);
        result.setQuotient(10000);
        result.setPartialDividend(new int[]{1234});
        result.setProduct(new int[]{1234});
        result.setRemainder("0035");

        String actualResult = formatter.format();

        assertEquals(expectedResult, actualResult);
    }

    String ReadTestDataFile(String file) {
        try {
            return new String(Objects.requireNonNull(getClass().getClassLoader()
                    .getResourceAsStream(file)).readAllBytes())
                    .replaceAll("\r\n", "\n");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
