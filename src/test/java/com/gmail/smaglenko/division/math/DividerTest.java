package com.gmail.smaglenko.division.math;

import com.gmail.smaglenko.division.model.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DividerTest {
    private Result result;
    private Divider divider;

    @BeforeEach
    public void setUp() {
        this.divider = new Divider();
    }

    @Test
    void divide_shouldRun_whenDividendAndDivisorTwoDigitNumbers() {
        int expectedResult = 9;

        result = divider.divide(99, 11);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldRun_whenDividendAndDivisorThreeDigitNumbers() {
        int expectedResult = 3;

        result = divider.divide(789, 234);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldRun_whenDividendAndDivisorFourDigitNumbers() {
        int expectedResult = 3;

        result = divider.divide(9999, 3333);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldRun_whenDividendAndDivisorNegativeNumbers() {
        int expectedResult = 13;

        result = divider.divide(-456, -33);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldRun_whenDividendAndDivisorMaxValueInt() {
        int expectedResult = 1;

        result = divider.divide(2147483647, 2147483647);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldRun_whenDividendLessDivisor() {
        int expectedResult = 0;

        result = divider.divide(200, 1000);
        int actualResult = result.getQuotient();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void divide_shouldThrowsArithmeticException_whenDivisorZero() {
        assertThrows(ArithmeticException.class, () ->
                divider.divide(400, 0));
    }

    @Test
    void divide_ThrowsArithmeticException_whenDividendZero() {
        assertThrows(ArithmeticException.class, () ->
                divider.divide(-0, 12));
    }

    @Test
    void divide_ThrowsNumberFormatException_whenQuotientNegative() {
        assertThrows(NumberFormatException.class, () ->
                divider.divide(-2147483648, 2147483647));
    }

    @Test
    void divide_shouldCreateEqualsObjectResult_whenAnotherObjectResultWithSameFields() {
        // Compare two objects Result one of them is created by divide method
        Result expectedResult = new Result();
        expectedResult.setDividend(78454);
        expectedResult.setDivisor(4);
        expectedResult.setQuotient(19613);
        expectedResult.setPartialDividend(new int[]{7, 38, 24, 5, 14});
        expectedResult.setProduct(new int[]{4, 36, 24, 4, 12});
        expectedResult.setRemainder("2");

        result = divider.divide(78454, 4);

        assertEquals(expectedResult, result);
    }

    @Test
    void calculatePartialDividendProductRemainder_shouldCalculatePartialDividendIndexByIndex() {
        int expectedResult = 7;

        result = new Result();
        result.setDividend(78454);
        result.setDivisor(4);
        result.setPartialDividend(new int[]{7});
        result.setProduct(new int[]{4});
        divider.calculatePartialDividendProductRemainder(7, result, 0);
        int actualResult = result.getPartialDividend()[0];

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void calculatePartialDividendProductRemainder_shouldCalculateProductIndexByIndex() {
        int expectedResult = 4;

        result = new Result();
        result.setDividend(78454);
        result.setDivisor(4);
        result.setPartialDividend(new int[]{7});
        result.setProduct(new int[]{4});
        divider.calculatePartialDividendProductRemainder(7, result, 0);
        int actualResult = result.getProduct()[0];

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void calculatePartialDividendProductRemainder_shouldCalculateRemainder() {
        String expectedResult = "3";

        result = new Result();
        result.setDividend(78454);
        result.setDivisor(4);
        result.setPartialDividend(new int[]{7, 38, 24, 5, 14});
        result.setProduct(new int[]{4, 36, 24, 4, 12});
        divider.calculatePartialDividendProductRemainder(7, result, 0);
        String actualResult = result.getRemainder();

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void removeAllEmptyCellArray_shouldRemoveAllEmptyCellArray() {
        int[] expectedResult = new int[]{1, 2, 3, 4};

        int[] actualResult = divider.removeAllEmptyCellArray(new int[]{1, 2, 0, 0, 3, 0, 4, 0, 0});

        assertArrayEquals(expectedResult, actualResult);
    }
}



