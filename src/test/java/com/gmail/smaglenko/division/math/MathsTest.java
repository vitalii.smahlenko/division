package com.gmail.smaglenko.division.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MathsTest {
    private Maths maths;

    @BeforeEach
    public void setUp() {
        this.maths = new Maths();
    }

    @Test
    void divide_shouldThrowsArithmeticException_whenDivisorZero() {
        assertThrows(ArithmeticException.class, () ->
                maths.divide(400, 0));
    }

    @Test
    void divide_ThrowsArithmeticException_whenDividendZero() {
        assertThrows(ArithmeticException.class, () ->
                maths.divide(0, 12));
    }

    @Test
    void divide_shouldRunWell_whenParameterInt() {
        int expectedResult = 148;

        int actualResult = maths.divide(2000158, 13452);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiple_shouldRunWell_whenParameterInt() {
        int expectedResult = 1136321640;

        int actualResult = maths.multiple(2000158, 13452);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiple_shouldIgnoreDigitalAfterCommas_whenParameterDouble() {
        int expectedResult = 313816;

        double actualResult = maths.multiple((int) 78454.20, (int) 4.5);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiple_shouldEqualsZero_whenMultiplicandZero() {
        int expectedResult = 0;

        int actualResult = maths.multiple(0, 500);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiple_shouldEqualsZero_whenMultiplierZero() {
        int expectedResult = 0;

        int actualResult = maths.multiple(1111, 0);

        assertEquals(expectedResult, actualResult);
    }
}
