package com.gmail.smaglenko.division.math;

/**
 * Class Maths - performs all non-standard math operations
 */
public class Maths {
    /**
     * Method divide is used instead of operands "/"
     */
    public int divide(int dividend, int divisor) {
        if (divisor == 0) {
            throw new ArithmeticException("Cannot divide by zero!");
        } else if (dividend == 0) {
            throw new ArithmeticException("If the dividend is \"0\", the result will be \"0\"!");
        } else {
            int result = 0;
            for (int i = 0; dividend >= divisor; i++) {
                dividend = dividend - divisor;
                result++;
            }
            return result;
        }
    }

    /**
     * Method multipleTwoNumbers is used instead of operands "*"
     */
    public int multiple(int multiplicand, int multiplier) {
        int product = 0;
        for (int i = 0; i < multiplicand; i++) {
            product = product + multiplier;
        }
        return product;
    }
}
