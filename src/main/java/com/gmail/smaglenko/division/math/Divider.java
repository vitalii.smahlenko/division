package com.gmail.smaglenko.division.math;

import com.gmail.smaglenko.division.model.Result;
import com.google.common.annotations.VisibleForTesting;

/**
 * math sub package - for performing math operations
 */
public class Divider {
    private Maths maths;
    private int tempRemainder;

    public Divider() {
        this.maths = new Maths();
    }

    /**
     * divide method performs division of numbers
     */
    public Result divide(int dividend, int divisor) {
        Result result = new Result();
        result.setDividend(Math.abs(dividend));
        result.setDivisor(Math.abs(divisor));
        result.setQuotient(maths.divide(result.getDividend(),
                result.getDivisor()));
        String dividendText = Integer.toString(result.getDividend());
        int startIndexPartialDividend = 0;
        for (int i = 1; i <= dividendText.length(); i++) {
            int tempPartialDividend =
                    Integer.parseInt
                            (dividendText.substring(startIndexPartialDividend, i));
            if (tempPartialDividend >= result.getDivisor()
                    && startIndexPartialDividend == 0) {
                /*
                 *  Specify the lengths of the arrays that store all partialDividend
                 *  and product values. To correctly determine the quantity of operations,
                 *  you need to subtract the length of the dividend from the length
                 *  of the partialDividend and plus 1
                 *  (because you need to compensate for the length of the partialDividend).
                 */
                int quantityOperation = dividendText.length()
                        - Integer.toString(tempPartialDividend).length() + 1;
                result.setPartialDividend(new int[quantityOperation]);
                result.setProduct(new int[quantityOperation]);
                calculatePartialDividendProductRemainder
                        (tempPartialDividend, result, 0);
                startIndexPartialDividend = i;
                // Find out all the rest of the partialDividend value, product and remainder
            } else if (startIndexPartialDividend > 0) {
                for (int j = 1; j <= result.getPartialDividend().length - 1; j++) {
                    tempPartialDividend =
                            Integer.parseInt(Integer.toString(tempRemainder)
                                    + dividendText.substring(startIndexPartialDividend, i));
                    if (tempPartialDividend >= result.getDivisor()) {
                        calculatePartialDividendProductRemainder
                                (tempPartialDividend, result, j);
                        startIndexPartialDividend = i;
                        i++;
                    } else if (tempPartialDividend < result.getDivisor()) {
                        if (dividendText.length() == i) { // If the incomplete dividend is less than the divisor, then we set it to the remainder.
                            result.setRemainder
                                    (dividendText.substring(startIndexPartialDividend, i));
                        } else {
                            i++;
                        }
                    }
                }
                result.setPartialDividend
                        (removeAllEmptyCellArray(result.getPartialDividend()));
                result.setProduct(removeAllEmptyCellArray(result.getProduct()));
            }
        }
        return result;
    }

    /**
     * calculatePartialDividendProductRemainder method - used to calculate
     * all values of partial dividend, product and remainder
     */
    @VisibleForTesting
    void calculatePartialDividendProductRemainder
    (int tempPartialDividend, Result result, int index) {
        // Find out the partialDividend value.
        result.getPartialDividend()[index] = tempPartialDividend;
        // Find out product.
        int greatestCommonDivisor
                = maths.divide(result.getPartialDividend()[index],
                result.getDivisor());
        result.getProduct()[index]
                = maths.multiple(result.getDivisor(),
                greatestCommonDivisor);
        // Find out the remainder
        tempRemainder = result.getPartialDividend()[index]
                - result.getProduct()[index];
        result.setRemainder(Integer.toString(tempRemainder));
    }

    @VisibleForTesting
    int[] removeAllEmptyCellArray(int[] oldArray) {
        int counterFullCell = 0;
        for (int i = 0; i < oldArray.length; i++) {
            if (oldArray[i] != 0) {
                int temp = oldArray[counterFullCell];
                oldArray[counterFullCell] = oldArray[i];
                oldArray[i] = temp;
                counterFullCell++;
            }
        }
        int[] arrayWithoutEmptyCell = new int[counterFullCell];
        for (int i = 0; i < arrayWithoutEmptyCell.length; i++) {

            arrayWithoutEmptyCell[i] = oldArray[i];
        }
        return arrayWithoutEmptyCell;
    }
}