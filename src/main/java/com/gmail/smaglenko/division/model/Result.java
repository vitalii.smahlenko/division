package com.gmail.smaglenko.division.model;

import java.util.Arrays;

/**
 * model sub package - for the classes that hold result data
 */
public class Result {
    private int quotient;
    private int dividend;
    private int divisor;
    private int[] partialDividend;
    private int[] product;
    private String remainder;

    public int getQuotient() {
        return quotient;
    }

    public void setQuotient(int quotient) {
        this.quotient = quotient;
    }

    public int getDividend() {
        return dividend;
    }

    public void setDividend(int dividend) {
        this.dividend = dividend;
    }

    public int getDivisor() {
        return divisor;
    }

    public void setDivisor(int divisor) {
        this.divisor = divisor;
    }

    public int[] getPartialDividend() {
        return partialDividend;
    }

    public void setPartialDividend(int[] partialDividend) {
        this.partialDividend = partialDividend;
    }

    public int[] getProduct() {
        return product;
    }

    public void setProduct(int[] product) {
        this.product = product;
    }

    public String getRemainder() {
        return remainder;
    }

    public void setRemainder(String remainder) {
        this.remainder = remainder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (quotient != result.quotient) return false;
        if (dividend != result.dividend) return false;
        if (divisor != result.divisor) return false;
        if (!Arrays.equals(partialDividend, result.partialDividend))
            return false;
        if (!Arrays.equals(product, result.product)) return false;
        return remainder != null ? remainder.equals(result.remainder) : result.remainder == null;
    }

    @Override
    public int hashCode() {
        int result = quotient;
        result = 31 * result + dividend;
        result = 31 * result + divisor;
        result = 31 * result + Arrays.hashCode(partialDividend);
        result = 31 * result + Arrays.hashCode(product);
        result = 31 * result + (remainder != null ? remainder.hashCode() : 0);
        return result;
    }
}