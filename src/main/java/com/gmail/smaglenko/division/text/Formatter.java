package com.gmail.smaglenko.division.text;

import com.gmail.smaglenko.division.math.Divider;
import com.gmail.smaglenko.division.model.Result;

/**
 * text sub package - for formatters
 */
public class Formatter {
    private Result result;
    private Divider divider;
    private int spaceNumber = 0;
    private String output;

    public Formatter(Result result) {
        this.result = result;
        this.divider = new Divider();
    }

    /**
     * format method creates a final String for outputting the result to the console
     */
    public String format() {
        // first row
        output = result.getDividend() + "|" + result.getDivisor() + "\n";
        // second row
        spaceNumber = Integer.toString(result.getPartialDividend()[0]).length()
                - Integer.toString(result.getProduct()[0]).length();
        output = output + getSpace(spaceNumber) + result.getProduct()[0]
                + getSpace(Integer.toString(result.getDividend()).length()
                - spaceNumber - Integer.toString(result.getProduct()[0]).length())
                + "|" + result.getQuotient() + "\n";
        // all following
        for (int i = 1; i < result.getPartialDividend().length; i++) {
            // If the remainder is not 0
            if ((result.getPartialDividend()[i - 1] - result.getProduct()[i - 1]) != 0) {
                spaceNumber = spaceNumber
                        + Integer.toString(result.getPartialDividend()[i - 1]).length()
                        - Integer.toString((result.getPartialDividend()[i - 1]
                        - result.getProduct()[i - 1])).length();
                output = output + getSpace(spaceNumber)
                        + result.getPartialDividend()[i] + "\n";
                addProductToOutput(i);
                // If the remainder is 0
            } else if ((result.getPartialDividend()[i - 1] - result.getProduct()[i - 1]) == 0) {
                spaceNumber = spaceNumber
                        + Integer.toString(result.getPartialDividend()[i - 1]).length();
                output = output + getSpace(spaceNumber)
                        + result.getPartialDividend()[i] + "\n";
                addProductToOutput(i);
            }
        }
        // last row
        spaceNumber = Integer.toString(result.getDividend()).length()
                - result.getRemainder().length();
        output = output + getSpace(spaceNumber) + result.getRemainder();
        return output;
    }

    private void addProductToOutput(int index) {
        if (Integer.toString(result.getPartialDividend()[index]).length()
                != Integer.toString(result.getProduct()[index]).length()) {
            spaceNumber = spaceNumber
                    + (Integer.toString(result.getPartialDividend()[index]).length()
                    - Integer.toString(result.getProduct()[index]).length());
            output = output + getSpace(spaceNumber) + result.getProduct()[index] + "\n";
        } else {
            output = output + getSpace(spaceNumber) + result.getProduct()[index] + "\n";
        }
    }

    /**
     * getSpace method to get the number of spaces you want
     */
    private String getSpace(int count) {
        String space = "";
        for (int i = 0; i < count; i++)
            space += " ";
        return space;
    }
}
