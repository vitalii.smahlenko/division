package com.gmail.smaglenko.division;

import com.gmail.smaglenko.division.exception.BadArgsException;
import com.gmail.smaglenko.division.math.Divider;
import com.gmail.smaglenko.division.model.Result;
import com.gmail.smaglenko.division.text.Formatter;

public class Context {
    private Class[] classes;
    private Result result;
    private Formatter formatter;
    private Divider divider;

    public Context(String[] args, Class... context) throws BadArgsException {
        this.classes = context;
        if (args.length != 2) {
            throw new BadArgsException("You can not use " + args.length
                    + " arguments. To perform division, you need to use "
                    + "2 arguments. For example "
                    + "java -jar division-1.0-SNAPSHOT.jar 1234 12");
        } else {
            for (Class c : classes) {
                //Create object class Result
                if (c == Result.class) {
                    this.result = divider.divide(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
                    //Create object class Divider
                } else if (c == Divider.class) {
                    this.divider = new Divider();
                    //Create object class Formatter
                } else if (c == Formatter.class) {
                    this.formatter = new Formatter(this.result);
                    System.out.println(formatter.format());
                } else {
                    System.out.println("Can not find such class like" + c);
                }
            }
            this.formatter.format();
        }
    }
}