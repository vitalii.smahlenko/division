package com.gmail.smaglenko.division;

import com.gmail.smaglenko.division.exception.BadArgsException;
import com.gmail.smaglenko.division.math.Divider;
import com.gmail.smaglenko.division.model.Result;
import com.gmail.smaglenko.division.text.Formatter;

/**
 * An application that divides numbers and prints result
 */
public class Main {
    public static void main(String[] args) {
        try {
            Context context = new Context(args, Divider.class, Result.class,
                    Formatter.class);
        } catch (BadArgsException e) {
            e.printStackTrace();
        }
    }
}
